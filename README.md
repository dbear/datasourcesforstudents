# dataSourcesForStudents

We have lots of data available to us. Some data is in csv form ready to use for simple excel assignments. Some is in more complex forms like json and yaml.  I want to make grabbing data easy. So here are some python formulations to do that.
## Great Sites
 - https://github.com/unitedstates/congress-legislators/ 
 - https://www.data.gov/local/
 - https://data.nasa.gov/
 
This is not just one project. Its a collection of python attacks on finding and repurposing data. Its also a memory aid. I hate writing code that I forget about and then years later have to solve the problem again. 