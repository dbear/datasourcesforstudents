#!/usr/bin/env python
import urllib
import requests
import json
from ruamel.yaml import YAML as yml
'''
By: DB
DT: 20190410

We want to grab data in json/yaml form and convert it to
something more usable in excel -- but how do we handle
nested data structures like lists in lists in lists? 

This is the first attempt to campture historical data
for us congress

we will start simple
'''
DATASRC0 = 'https://github.com/unitedstates/congress-legislators/'
DATASRC1 = 'https://theunitedstates.io/congress-legislators/legislators-historical.yaml'

if __name__=="__main__":
    print("Started")
    remoteData = requests.get(DATASRC1)
    print("Status: ", remoteData.status_code)
    print("Encoding: ", remoteData.encoding)
    yamldata = remoteData.text
    print("Length ", len(yamldata))
